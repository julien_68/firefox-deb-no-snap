#!/bin/sh
echo 'Ce script est basé sur le tuto de numétopia => https://www.numetopia.fr/comment-installer-firefox-au-format-deb-sur-ubuntu/'
sleep 1
echo 'Go'
sleep 2
sudo snap remove firefox
sudo apt remove firefox* -y
sudo add-apt-repository ppa:mozillateam/ppa -y
echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla-firefox
sudo apt install firefox -y
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
echo 'Changement terminer'
echo 'Hesiter pas a aller voir le site du numétopia'
